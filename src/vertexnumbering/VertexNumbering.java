package vertexnumbering;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import models.Graph;
import models.MIP;
import models.RKGA;

/**
 * VertexNumbering compares two solutions (MIP model and random key GA) for
 * a problem posted in OR Stack Exchange.
 *
 * Given a layered graph, the problem calls for labeling vertices in each layer
 * with integers from 1 to the number of vertices in the layer, so as to
 * minimize the sum across all edges of the squared differences in the labels
 * assigned to endpoints of each edge.
 *
 * The question, and a MIP model suggested by Rob Pratt, are posted at
 * https://or.stackexchange.com/questions/6519/numbering-the-vertices-of-an
 * -n-layer-graph-so-that-edges-have-similar-numbered.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class VertexNumbering {

  /**
   * Dummy constructor.
   */
  private VertexNumbering() { }

  /**
   * Generates a random graph instance and runs the various models.
   *
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set problem dimensions.
    int nLayers = 5;      // number of layers to include in the graph
    int minVertices = 3;  // minimum number of vertices in a layer
    int maxVertices = 9;  // maximum number of vertices in a layer
    double pEdge = 0.25;  // probability that any given possible edge exists
    long seed = 1312;     // seed for the ranbdom number generator

    // Set parameters for the genetic algorithm.
    int nIslands = 5;     // number of islands to use
    double pMut = 0.02;   // mutation probability
    int nCrossover = 2;   // number of crossover points
    int popSize = 100;    // population per island
    int elite = 5;        // number of elites to retain per island
    int epoch = 40;       // how often migrations occur
    int migrants = 5;     // migrants per island
    int stagnation = 500; // max generations with no improvement
    int restarts = 10;    // number of GA runs to perform

    // Set a time limit for both methods (in seconds).
    double timeLimit = 300;

    // Generate a random graph instance.
    Graph g = new Graph(nLayers, minVertices, maxVertices, pEdge, seed);
    System.out.println(g);

    // Build and run a random key genetic algorithm.
    RKGA ga = new RKGA(g, nIslands, pMut, nCrossover);
    System.out.println("\nTrying a GA ...\n");
    long clock = System.currentTimeMillis();
    ga.setSeed(seed);
    int[] incumbent = null;
    long incValue = Long.MAX_VALUE;
    // Because the GA time limit resets with each restart, limit each restart
    // to the remaining time.
    for (int i = 0; i < restarts; i++) {
      System.out.println("... GA run " + i + " ...");
      double tl = timeLimit - (System.currentTimeMillis() - clock) / 1000;
      if (tl <= 0) {
        break;
      }
      int[] solution =
        ga.evolve(popSize, elite, epoch, migrants, tl, stagnation);
      long value = g.getObjValue(solution);
      if (value < incValue) {
        incValue = value;
        incumbent = solution;
      }
    }
    clock = System.currentTimeMillis() - clock;
    System.out.println(g.evaluate(incumbent));
    System.out.println("GA run time = " + clock + " ms.");

    // Build and solve a MIP model.
    System.out.println("\nTrying a MIP model ...\n");
    try (MIP mip = new MIP(g)) {
      // Use the GA found an incumbent as a starting solution.
      if (incumbent != null) {
        mip.setStart(incumbent);
      }
      IloCplex.Status status = mip.solve(timeLimit);
      System.out.println("Final solver status = " + status);
      System.out.println(g.evaluate(mip.getLabels()));
    } catch (IloException ex) {
      System.out.println("MIP model failed:\n" + ex.getMessage());
    }
  }
}
