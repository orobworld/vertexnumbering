package models;

import java.util.ArrayList;
import java.util.Random;

/**
 * Graph contains an instance of a layered graph.
 *
 * Vertices are numbered from 0 to n-1, where n is the number of vertices in
 * the graph. Vertices in layer i are *labeled* from 1 to n_i, where n_i is
 * the number of vertices in layer i.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Graph {
  // Dimension labels for the layers matrix.
  private static final int SIZE = 0;   // number of vertices in the layer
  private static final int FIRST = 1;  // index of the first vertex in the layer
  private static final int LAST = 2;   // index of the last vertex in the layer

  // Dimension labels for the edges matrix.
  private static final int TAIL = 0;   // index of the tail node
  private static final int HEAD = 1;   // index of the head node
  private static final int FROM = 2;   // index of the layer containing the tail

  private final int nVertices;         // number of vertices
  private final int nLayers;           // number of layers
  private final int nEdges;            // number of edges
  private final int minLayerSize;      // minimum layer size
  private final int maxLayerSize;      // maximum layer size
  private final int[][] layers;        // info about each layer
  private final int[][] edges;         // info about each edge

  /**
   * Constructs a random graph.
   * @param nL the number of layers to use
   * @param minV the minimum number of vertices in a layer
   * @param maxV the maximum number of vertices in a layer
   * @param p the proportion of possible arcs that actually exist
   * @param seed the seed for the random number generator
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public Graph(final int nL, final int minV, final int maxV, final double p,
               final long seed) {
    nLayers = nL;
    // Set up a random number generator.
    Random rng = new Random(seed);
    // Set up the layers matrix.
    layers = new int[nLayers][3];
    // Assign nodes to each layer.
    int id = 0;
    int mv = maxV - minV + 1;
    int s1 = Integer.MAX_VALUE;     // tracks min layer size
    int s2 = 0;                     // tracks max layer size
    for (int j = 0; j < nL; j++) {
      // Randomly choose the number of nodes to put in the layer.
      int n = minV + rng.nextInt(mv);
      s1 = Math.min(s1, n);
      s2 = Math.max(s2, n);
      // Assign the nodes.
      layers[j][SIZE] = n;
      layers[j][FIRST] = id;
      layers[j][LAST] = id + n - 1;
      id += n;
    }
    nVertices = id;
    minLayerSize = s1;
    maxLayerSize = s2;
    // Since the number of edges is initially undetermined, use two array lists
    // to track their endpoints.
    ArrayList<Integer> heads = new ArrayList<>();
    ArrayList<Integer> tails = new ArrayList<>();
    ArrayList<Integer> from = new ArrayList<>();
    // Consider each possible edge and randomly decide whether to include it.
    for (int i = 0; i < nLayers - 1; i++) {
      for (int j = layers[i][FIRST]; j <= layers[i][LAST]; j++) {
        for (int k = layers[i + 1][FIRST]; k <= layers[i + 1][LAST]; k++) {
          if (rng.nextDouble() <= p) {
            // Take the edge from j to k.
            tails.add(j);
            heads.add(k);
            from.add(i);
          }
        }
      }
    }
    // Convert the edge info into a matrix.
    edges = new int[tails.size()][3];
    for (int i = 0; i < tails.size(); i++) {
      edges[i][TAIL] = tails.get(i);
      edges[i][HEAD] = heads.get(i);
      edges[i][FROM] = from.get(i);
    }
    nEdges = edges.length;
  }

  /**
   * Gets the number of vertices in the graph.
   * @return the vertex count
   */
  public int getNVertices() {
    return nVertices;
  }

  /**
   * Gets the number of layers in the graph.
   * @return the layer count
   */
  public int getNLayers() {
    return nLayers;
  }

  /**
   * Gets the number of edges in the graph.
   * @return the edge count
   */
  public int getNEdges() {
    return nEdges;
  }

  /**
   * Gets the maximum number of vertices in any layer.
   * This is also the largest possible label value.
   * @return the maximum layer size
   */
  public int getMaxLayerSize() {
    return maxLayerSize;
  }

  /**
   * Gets the size (vertex count) of a layer.
   * @param layerIndex the index of the layer
   * @return the number of vertices in the layer
   */
  public int getLayerSize(final int layerIndex) {
    return layers[layerIndex][SIZE];
  }

  /**
   * Gets the sizes of all layers.
   * @return the layer sizes
   */
  public int[] getLayerSizes() {
    return getColumn(layers, SIZE);
  }

  /**
   * Gets the lowest vertex index in a layer.
   * @param layerIndex the index of the target layer
   * @return the index of the first vertex in the layer
   */
  public int getLayerStart(final int layerIndex) {
    return layers[layerIndex][FIRST];
  }

  /**
   * Gets the first vertex in every layer.
   * @return the initial vertices of layers
   */
  public int[] getLayerStarts() {
    return getColumn(layers, FIRST);
  }

  /**
   * Gets the highest vertex index in a layer.
   * @param layerIndex the index of the target layer
   * @return the index of the last vertex in the layer
   */
  public int getLayerEnd(final int layerIndex) {
    return layers[layerIndex][LAST];
  }

  /**
   * Gets the last vertex in every layer.
   * @return the final vertices of layers
   */
  public int[] getLayerEnds() {
    return getColumn(layers, LAST);
  }

  /**
   * Gets the head vertex of an edge.
   * @param edgeIndex the index of the edge
   * @return the index of the head vertex
   */
  public int getHead(final int edgeIndex) {
    return edges[edgeIndex][HEAD];
  }

  /**
   * Gets the tail vertex of an edge.
   * @param edgeIndex the index of the edge
   * @return the index of the tail vertex
   */
  public int getTail(final int edgeIndex) {
    return edges[edgeIndex][TAIL];
  }

  /**
   * Generates a summary of the graph.
   * @return a string summarizing the graph
   */
  public String toString() {
    StringBuilder sb = new StringBuilder("Graph dimensions:\n");
    sb.append("\tOverall vertex count = ").append(nVertices).append(".\n")
      .append("\tNumber of layers = ").append(nLayers).append(".\n")
      .append("\tSmallest layer has ").append(minLayerSize)
      .append(" vertices.\n\tLargest layer has ").append(maxLayerSize)
      .append(" vertices.\n\tNumber of edges = ").append(nEdges).append(".");
    return sb.toString();
  }

  /**
   * Evaluates a candidate solution.
   * @param labels the labels for every node
   * @return a string report of the solution
   */
  public String evaluate(final int[] labels) {
    StringBuilder sb = new StringBuilder();
    // Verify that no label exceeds the layer maximum and that every label
    // in a layer is used at most once.
    for (int k = 0; k < nLayers; k++) {
      int s = layers[k][SIZE];
      boolean[] used = new boolean[s + 1];
      for (int i = layers[k][FIRST]; i <= layers[k][LAST]; i++) {
        int n = labels[i];
        if (n < 1) {
          sb.append("Invalid label ").append(n).append(" encountered in layer ")
            .append(k).append("\n");
        } else if (n > s) {
          sb.append("Label ").append(n).append(" in layer ").append(k)
            .append(" exceeds layer size ").append(s).append(".\n");
        } else if (used[n]) {
          sb.append("Label ").append(n).append(" already used in layer ")
            .append(k).append(".\n");
        } else {
          used[n] = true;
        }
      }
    }
    // Evaluate the objective value.
    sb.append("Objective value = ").append(getObjValue(labels)).append(".");
    return sb.toString();
  }

  /**
   * Computes the objective value of a vector of vertex labels.
   * @param labels the vertex labels
   * @return the objective value
   */
  public long getObjValue(final int[] labels) {
    long obj = 0;
    for (int e = 0; e < edges.length; e++) {
      int a = labels[edges[e][TAIL]];
      a -= labels[edges[e][HEAD]];
      obj += a * a;
    }
    return obj;
  }

  /**
   * Extracts a specified column from an integer matrix (either the edge
   * matrix or the layer matrix).
   * @param m the source matrix
   * @param col the column to extract
   * @return the extracted column
   */
  private int[] getColumn(final int[][] m, final int col) {
    int[] c = new int[m.length];
    for (int i = 0; i < m.length; i++) {
      c[i] = m[i][col];
    }
    return c;
  }
}
