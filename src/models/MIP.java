package models;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloQuadNumExpr;
import ilog.cplex.IloCplex;
import java.util.ArrayList;

/**
 * MIP implements a mixed-integer programming model for the vertex
 * labeling problem.
 *
 * The model is based on Rob Pratt's solution to the OR SE question.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private static final double HALF = 0.5;  // rounding cutoff
  private final IloCplex mip;              // MIP model
  private final IloIntVar[][] label;       // indicators for label assignments
    // label[i][j] = 1 if vertex i is assigned label j + 1

  /**
   * Constructs the MIP model.
   * @param g the graph to be processed
   * @throws IloException if CPLEX balks at any step
   */
  public MIP(final Graph g) throws IloException {
    // Get key dimensions.
    int nVertices = g.getNVertices();
    int nEdges = g.getNEdges();
    int maxLabel = g.getMaxLayerSize();
    int nLayers = g.getNLayers();
    // Initialize the MIP model object.
    mip = new IloCplex();
    // Create indicator variables for label assignments.
    label = new IloIntVar[nVertices][maxLabel];
    for (int i = 0; i < nVertices; i++) {
      for (int j = 0; j < maxLabel; j++) {
        label[i][j] = mip.boolVar("x_" + i + "_" + (j + 1));
      }
    }
    // Disallow assignments to labels greater than the layer size.
    for (int k = 0; k < nLayers; k++) {
      int n = g.getLayerSize(k);
      if (n < maxLabel) {
        for (int i = g.getLayerStart(k); i <= g.getLayerEnd(k); i++) {
          for (int j = n; j < maxLabel; j++) {
            label[i][j].setUB(0);
          }
        }
      }
    }
    // The objective is to minimize the sum of the edge costs.
    IloQuadNumExpr obj = mip.quadNumExpr();
    for (int e = 0; e < nEdges; e++) {
      // Get the endpoints of edge e.
      int head = g.getHead(e);
      int tail = g.getTail(e);
      for (int i = 0; i < maxLabel; i++) {
        for (int j = 0; j < maxLabel; j++) {
          // Compute the cost of this pair of labels.
          double c = (i - j) * (i - j);
          // Add the cost times the product of the two indicators.
          obj.addTerm(c, label[head][i], label[tail][j]);
        }
      }
    }
    mip.addMinimize(obj);
    // Every vertex needs to be assigned exactly one label.
    for (int i = 0; i < nVertices; i++) {
      mip.addEq(mip.sum(label[i]), 1.0);
    }
    // Every valid label (1, ..., layer size) needs to be assigned to exactly
    // one vertex in the layer.
    for (int k = 0; k < nLayers; k++) {
      // Get the size of the layer. This is the maximum valid label.
      int n = g.getLayerSize(k);
      for (int j = 0; j < n; j++) {
        // Assign label j to exactly one vertex in the layer.
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (int i = g.getLayerStart(k); i <= g.getLayerEnd(k); i++) {
          expr.addTerm(1.0, label[i][j]);
        }
        mip.addEq(expr, 1.0);
      }
    }
  }

  /**
   * Attempts to solve the MIP model.
   * @param timeLimit the time limit in seconds for the solver
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    mip.solve();
    return mip.getStatus();
  }

  /**
   * Closes the MIP model.
   */
  @Override
  public void close() {
    mip.close();
  }

  /**
   * Gets the labels assigned by the incumbent solution to the MIP model.
   * @return the incumbent labels
   * @throws IloException if there is no incumbent or it cannot be accessed
   */
  public int[] getLabels() throws IloException {
    IloCplex.Status status = mip.getStatus();
    if (status == IloCplex.Status.Optimal
        || status == IloCplex.Status.Feasible) {
      int[] sol = new int[label.length];
      for (int i = 0; i < label.length; i++) {
        double[] z = mip.getValues(label[i]);
        for (int j = 0; j < z.length; j++) {
          if (z[j] > HALF) {
            sol[i] = j + 1;
            break;
          }
        }
      }
      return sol;
    } else {
      throw new IloException("Cannot recover solution when status = " + status);
    }
  }

  /**
   * Sets a starting solution for the MIP.
   * @param labels the labels to use for the starting solution
   * @throws IloException if the MIP start is rejected
   */
  public void setStart(final int[] labels) throws IloException {
    int mx = label[0].length;  // max # of possible labels
    ArrayList<IloNumVar> varList = new ArrayList<>();
    ArrayList<Double> valList = new ArrayList<>();
    for (int i = 0; i < label.length; i++) {
      for (int j = 0; j < mx; j++) {
        varList.add(label[i][j]);
        double x = (labels[i] == (j + 1)) ?  1.0 : 0.0;
        valList.add(x);
      }
    }
    IloNumVar[] vars = varList.toArray(new IloNumVar[varList.size()]);
    double[] vals = valList.stream().mapToDouble(x -> x).toArray();
    mip.addMIPStart(vars, vals, IloCplex.MIPStartEffort.Auto);
  }
}
