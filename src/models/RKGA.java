package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import org.uncommons.watchmaker.framework.islands.IslandEvolution;
import org.uncommons.watchmaker.framework.islands.IslandEvolutionObserver;
import org.uncommons.watchmaker.framework.islands.RingMigration;
import org.uncommons.watchmaker.framework.operators.DoubleArrayCrossover;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.ElapsedTime;
import org.uncommons.watchmaker.framework.termination.Stagnation;
import org.uncommons.watchmaker.framework.termination.TargetFitness;
import valueorderedmap.ValueOrderedMap;

/**
 * RKGA implements a random key genetic algorithm for the vertex numbering
 * problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class RKGA implements FitnessEvaluator<double[]> {
  private static final long SECTOMS = 1000;
  private final Graph graph;    // the graph to label
  private final int nVertices;  // number of vertices in the graph
  private final int nLayers;    // number of layers in the graph
  private final int[] first;    // the first vertex of every layer
  private final int[] last;     // the last vertex of every layer
  private final IslandEvolution<double[]> engine;  // the evolutionary engine
  private final MersenneTwisterRNG rng;            // the random no. generator

  /**
   * Constructs the GA instance.
   * @param g the graph to label
   * @param nIslands the number of islands to use
   * @param pMut the mutation probability
   * @param nCross number of crossover points
   */
  public RKGA(final Graph g, final int nIslands,
              final double pMut, final int nCross) {
    graph = g;
    nVertices = g.getNVertices();
    nLayers = g.getNLayers();
    first = g.getLayerStarts();
    last = g.getLayerEnds();
    // Create the random number generator.
    rng = new MersenneTwisterRNG();
    // Create the chromosome factory.
    CandidateFactory<double[]> factory = new ChromosomeFactory(nVertices);
    // Create the operators.
    List<EvolutionaryOperator<double[]>> operators =
      new ArrayList<EvolutionaryOperator<double[]>>(nVertices);
    operators.add(new DoubleArrayCrossover(nCross));
    operators.add(new Mutator(new Probability(pMut)));
    EvolutionaryOperator<double[]> pipeline =
      new EvolutionPipeline<double[]>(operators);
    // Set up the evolutionary engine.
    engine = new IslandEvolution<>(nIslands,
                                   new RingMigration(),
                                   factory,
                                   pipeline,
                                   this,
                                   new RouletteWheelSelection(),
                                   rng);
    // Add a logger.
    engine.addEvolutionObserver(new Logger());
  }

  /**
   * Converts a chromosome into a vector of vertex labels.
   * @param chromosome the chromosome to decode
   * @return the corresponding labels
   */
  private int[] decode(final double[] chromosome) {
    ValueOrderedMap<Integer, Double> map = new ValueOrderedMap<>();
    int[] labels = new int[nVertices];
    // Proceed layer by layer.
    for (int k = 0; k < nLayers; k++) {
      map.clear();
      // Map the indices in the layer to their chromosome values.
      for (int i = first[k]; i <= last[k]; i++) {
        map.put(i, chromosome[i]);
      }
      // Generate a list of vertices in the layer in chromosome order.
      int[] sorted = map.ascendingKeyStream().mapToInt(i -> i).toArray();
      // Label the vertices.
      int j = 1;
      for (int i = 0; i < sorted.length; i++) {
        labels[sorted[i]] = j;
        j += 1;
      }
    }
    return labels;
  }

  /**
   * Evaluates the fitness of a candidate.
   * @param candidate the candidate solution
   * @param population the population of solutions (ignored)
   * @return the objective value of the candidate
   */
  @Override
  public double getFitness(final double[] candidate,
                           final List<? extends double[]> population) {
    int[] labels = decode(candidate);
    return graph.getObjValue(labels);
  }

  /**
   * Checks whether the fitness function is "natural" (fitter candidates
   * have higher scores) or not.
   * @return false (lower fitness values are preferable)
   */
  @Override
  public boolean isNatural() {
    return false;
  }

  /**
   * Seeds the random number generator.
   * @param seed the new seed
   */
  public void setSeed(final long seed) {
    rng.setSeed(seed);
  }

  /**
   * Runs the evolutionary engine.
   * @param popSize the population size per island to use
   * @param elite the number of elite candidates to retain per island
   * @param epoch the number of epochs between migrations
   * @param migrations the number of migrants per island
   * @param timeLimit time limit in seconds
   * @param stagnation max generations without improvement
   * @return the best candidate found
   */
  public int[] evolve(final int popSize, final int elite, final int epoch,
                      final int migrations, final double timeLimit,
                      final int stagnation) {
    long tl = Math.round(SECTOMS * timeLimit);
    // Run the evolutionary engine.
    double[] sol =
      engine.evolve(popSize, elite, epoch, migrations,
                    new TargetFitness(0, false),       // stop on fitness = 0
                    new ElapsedTime(tl),               // stop at time limit
                    new Stagnation(stagnation, false)  // stop on stagnation
      );
    // Decode and return the best solution.
    return decode(sol);
  }

  /**
   * ChromosomeFactory is a factory for generating candidate solutions
   * (chromosomes).
   */
  private static class ChromosomeFactory
                 extends AbstractCandidateFactory<double[]>
                 implements CandidateFactory<double[]> {

    private final int length; // chromosome length

    /**
     * Constructs the factory.
     * @param dimension the dimension of a chromosome vector (# of vertices)
     */
    ChromosomeFactory(final int dimension) {
      length = dimension;
    }

    /**
     * Generates a new chromosome randomly.
     * @param rng the random number generator to use
     * @return the new chromosome
     */
    @Override
    public double[] generateRandomCandidate(final Random rng) {
      return rng.doubles(length).toArray();
    }
  }

  /**
   * Mutator handles mutations of chromosomes.
   */
  private static class Mutator implements EvolutionaryOperator<double[]> {
    private final Probability p;  // the mutation probability

    /**
     * Constructs the mutator.
     * @param prob the mutation probability.
     */
    Mutator(final Probability prob) {
      p = prob;
    }

    /**
     * Mutates a list of chromosomes.
     * @param selectedCandidates the chromosomes to mutate
     * @param rng the random number generator to use
     * @return the mutated chromosomes
     */
    @Override
    public List<double[]> apply(final List<double[]> selectedCandidates,
                                final Random rng) {
      List<double[]> newCandidates =
        new ArrayList<double[]>(selectedCandidates.size());
      newCandidates.addAll(selectedCandidates);
      for (int i = 0; i < newCandidates.size(); i++) {
        if (p.nextEvent(rng)) {
          double[] mutated = newCandidates.get(i);
          int dimension = rng.nextInt(mutated.length);
          mutated[dimension] += rng.nextGaussian();
          newCandidates.set(i, mutated);
        }
      }
      return newCandidates;
    }
  }

  /**
   * Logger provides a mechanism to track progress in the standard output
   * stream.
   */
  private static class Logger implements IslandEvolutionObserver<double[]> {
    private static final int PERIOD = 100;  // number of generations between
                                            // prints

    /**
     * Prints a progress report.
     * @param data the current population data
     */
    @Override
    public void populationUpdate(
                  final PopulationData<? extends double[]> data) {
      int g = data.getGenerationNumber();
      if (g % PERIOD == 0) {
        System.out.printf("Generation %d: %f\n",
                          g,
                          data.getBestCandidateFitness());
      }
    }

    /**
     * Receives progress information island by island.
     * Island-specific information is ignored.
     * @param islandIndex the index of the island being reported
     * @param data the population data
     */
    @Override
    public void islandPopulationUpdate(final int islandIndex,
                                       final PopulationData<? extends double[]>
                                         data) {
    }

  }
}
