# Vertex Numbering #

### What is this repository for? ###

A [question](https://or.stackexchange.com/questions/6519/numbering-the-vertices-of-an-n-layer-graph-so-that-edges-have-similar-numbered) on OR Stack Exchange deals with labeling vertices in a layered graph with integers so as to minimize the sum over all edges of the squared difference of the labels at either end of each edge. I'm using "labeling" rather than "numbering" because the latter suggests a sequence $`1,\dots, \vert V \vert`$ of vertex numbers (where $`V`$ is the vertex set of the full graph), whereas what is intended is that vertices in layer $`i`$ be numbered $`1,\dots, n_i`$ where $`n_i`$ is the number of vertices in layer $`i`$.

Rob Pratt proposed a binary integer program (IP) with a quadratic objective and linear constraints, which is guaranteed to provide an exact solution given enough time and memory. Since indexing the vertices within a layer is equivalent to picking a permutation of them, I wanted to try a random key genetic algorithm (RKGA). RKGAs were originally designed for sequencing problems, so the fit is natural. I used an "island" GA to try to stave off premature stagnation, but a more conventional GA might work just as well.

This repository contains my Java code for generating a test problem and solving it using both the IP model and the RKGA method. The code uses three libraries not included here:

- CPLEX 20.1, to solve the IP model;
- [Watchmaker Framework 0.7.1](https://watchmaker.uncommons.org/), to implement the genetic algorithm; and
- [ValueOrderedMap](https://gitlab.msu.edu/orobworld/valueorderedmap),  a Java library I wrote, to sort double[] vectors and recover the permutation index for the sort (used for decoding a "chromosome" into permutations of the vertices in each layer).


### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

